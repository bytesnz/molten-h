import React from 'react';
import { withKnobs, number, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { Form } from '../src/components/Form';

export default {
  title: 'Form',
  component: Form,
  decorators: [withKnobs]
};

const storyConfig = {
  parameters: {
    knobs: {
      escapeHTML: false
    }
  }
}

export const SingleStringField = () => {
  let value = 'This is a string';
  let initialSchema = `{
    "type": "string",
    "title": "A string",
    "maxLength": 5
  }`;

  const schema = text('schema', initialSchema);

  console.log('schema is', initialSchema, schema);

  return (
    <Form schema={JSON.parse(schema)} value={value} />
  )
};

SingleStringField.story = storyConfig;

export const SimpleObjectField = () => {
  let value = {
    string: 'This is a string',
    number: 20
  };
  let initialSchema = `{
    "type": "object",
    "properties": {
      "string": {
        "type": "string",
        "title": "A string",
        "maxLength": 5
      },
      "number": {
        "type": "number",
        "title": "Some number",
        "description": "with a description of course",
        "maximum": 42,
        "minimum": 3.1415
      }
    }
  }`;

  const schema = text('schema', initialSchema);

  console.log('schema is', initialSchema, schema);

  return (
    <Form schema={JSON.parse(schema)} value={value} />
  )
};

SimpleObjectField.story = storyConfig;

