import React from 'react';
import { withKnobs, number, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
import { createComponents } from '../src/components/string';
import { ValueStore } from './ValueStore';

const components = createComponents(React.createElement);
const Text = components.input;

export default {
  title: 'String Field',
  component: Text,
  decorators: [withKnobs]
};


let value = 'This is a string';
export const string = () => {
  const minLength = number('minLength', 0);
  const maxLength = number('maxLength', 20);
  const pattern = text('pattern', '');
  const schema = {
    type: 'string',
    title: 'String Type',
    minLength,
    maxLength,
    pattern
  };

  return (
    <ValueStore schema={schema} Component={Text}
      initialValue={value} />
  )
};

