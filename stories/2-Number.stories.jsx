import React from 'react';
import { withKnobs, number, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { createComponents } from '../src/components/number';
import { ValueStore } from './ValueStore';

const components = createComponents(React.createElement);
const NumberField = components.input;

export default {
  title: 'Number Field',
  component: NumberField,
  decorators: [withKnobs]
};

let numberValue = 3.1415;
const numberField = () => {
  const minimum = number('minimum', 0);
  const exclusiveMinimum = number('exclusiveMinimum');
  const maximum = number('maximum', 20);
  const exclusiveMaximum = number('exclusiveMaximum');
  const multipleOf = number('multipleOf');
  const schema = {
    type: 'number',
    title: 'Number Type',
    minimum,
    exclusiveMinimum,
    maximum,
    exclusiveMaximum
  };

  return (
    <ValueStore schema={schema} Component={NumberField}
      initialValue={numberValue} />
  )
};
export { numberField as number };

let integerValue = 42;
export const integer = () => {
  const minimum = number('minimum', 0);
  const exclusiveMinimum = number('exclusiveMinimum');
  const maximum = number('maximum', 20);
  const exclusiveMaximum = number('exclusiveMaximum');
  const multipleOf = number('multipleOf');
  const schema = {
    type: 'integer',
    title: 'Integer Type'
  };

  console.log('exclusiveMinimum', exclusiveMinimum);
  if (typeof minimum === 'number') {
    schema.minimum = minimum;
  }
  if (typeof exclusiveMinimum === 'number') {
    schema.exclusiveMinimum = exclusiveMinimum;
  }
  if (typeof maximum === 'number') {
    schema.maximum = maximum;
  }
  if (typeof exclusiveMaximum === 'number') {
    schema.exclusiveMaximum = exclusiveMaximum;
  }
  if (typeof mutlipleOf === 'number') {
    schema.mutlipleOf = mutlipleOf;
  }

  return (
    <ValueStore schema={schema} Component={NumberField}
      initialValue={integerValue} />
  )
};
