import React, { useState, createElement } from 'react';
import Ajv from 'ajv';

const ajv = new Ajv();

export const ValueStore = ({initialValue, schema, Component}) => {
  const [value, setValue] = useState(initialValue);

  const onChange = (newValue) => {
    setValue(newValue);
  };

  let errors;
  const valid = ajv.validate(schema, value);
  if (!valid) {
    errors = ajv.errorsText();
  }

  return (
    <label>
      {schema.title || ''}
      <div>
        <Component schema={schema} value={value} onChange={onChange} />
      </div>
      <div>
        { errors || '' }
      </div>
    </label>
  );
};
