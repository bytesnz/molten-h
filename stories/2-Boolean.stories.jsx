import React from 'react';
import { action } from '@storybook/addon-actions';
import { createComponents } from '../src/components/boolean';
import { ValueStore } from './ValueStore';

const components = createComponents(React.createElement);
const BooleanField = components.input;

export default {
  title: 'Boolean Field',
  component: BooleanField
};

const boolean = () => {
  const schema = {
    type: 'boolean',
    title: 'Boolean Type'
  };

  return (
    <ValueStore schema={schema} Component={BooleanField} />
  )
};

