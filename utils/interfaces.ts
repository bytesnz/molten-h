import dtsGenerator, { DefaultTypeNameConvertor, SchemaId } from 'dtsgenerator';
import { writeFile as asyncWriteFile } from 'fs';
import { promisify } from 'util';
import { resolve } from 'path';

const writeFile = promisify(asyncWriteFile);

const interfaces = [
  {
    files: ['../schemas/json-schema.json'],
    namespace: 'JFORMS',
    output: '../typings/JsonSchema.d.ts'
  }
];

interfaces.forEach((config) => {
  dtsGenerator({
    contents: config.files.map((file) => require(file)),
    namespaceName: config.namespace,
    typeNameConvertor: (id: SchemaId) => {
      let names = DefaultTypeNameConvertor(id);
      if (names.length > 0) {
        names = names.map((part) => {
          if (part.length > 4 && part.endsWith('Json')) {
            return part.slice(0, -4);
          }
          return part;
        });
      }
      return names;
    }
  }).then((result) => {
    // console.log(result);
    console.log('Writing type definition file ', config.output);
    return writeFile(resolve(__dirname, config.output), result);
  });
});
