// Load ts-node
require('ts-node').register();

const test = require('japa');
const { configure } = test;

let cleanup;

configure({
  files: process.argv.length > 2 ? process.argv.slice(2) : ['**/*.test.tsx?'],
});

test.beforeEach(() => {
  cleanup = require('jsdom-global')();
});

test.afterEach(() => {
  cleanup();
});
