interface InputProps {
  schema: { [key: string]: any },
  value?: string | null,
  required?: boolean;
  onChange?: (value: string) => void;
}

export const createComponents = (h) => {
  return {
    input: (props: InputProps) => {
      const { schema, value, required, onChange } = props;
      let type: string;
      switch (schema.format) {
        case 'email':
        case 'idn-email':
          type = 'email';
          break;
        case 'hostname':
        case 'hostname.idn':
        default:
          type = 'string';
      }

      const handleChange = (event) => {
        if (onChange) {
          onChange(event.target.value);
        }
      };

      const attributes = {
        type,
        minLength: typeof schema.minLength === 'number' ? schema.minLength : null,
        maxLength: typeof schema.maxLength === 'number' ? schema.maxLength : null,
        required: required === true || null,
        pattern: schema.pattern || null,
        value: value || '',
        onChange: handleChange
      };

      return h('input', attributes);
    }
  };
};
export default createComponents;
