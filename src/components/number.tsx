interface InputProps {
  schema: { [key: string]: any },
  value?: number| null,
  required?: boolean;
  onChange?: (value: number) => void;
}

export const createComponents = (h) => {
  return {
    input: (props: InputProps) => {
      const { schema, value, required, onChange } = props;
      console.log('render called');
      let pattern = '\\d+';

      if (schema.type === 'number') {
        pattern += '(\\.\\d+)?';
      }
      if (schema.maximum <= 0) {
        pattern = '-' + pattern;
      } else if (schema.minimum < 0) {
        pattern = '-?' + pattern;
      }

      const handleChange = (event) => {
        console.log('handle change called');
        const newValue = Number(event.target.value);

        if (!isNaN(newValue)) {
          if (schema.exclusiveMaximum && newValue >= schema.exclusiveMaximum) {
            event.target.setCustomValidity('should be < ' + schema.exclusiveMaximum);
          } else if (schema.maximum && newValue > schema.maximum) {
            event.target.setCustomValidity('should be <= ' + schema.maximum);
          } else if (schema.exclusiveMimimum && newValue <= schema.exclusiveMinimum) {
            event.target.setCustomValidity('should be > ' + schema.exclusiveMinimum);
          } else if (schema.minimum && newValue < schema.minimum) {
            event.target.setCustomValidity('should be >= ' + schema.minimum);
          } else {
            event.target.setCustomValidity('');
          }
        }

        if (onChange) {
          onChange(isNaN(newValue) ? event.target.value : newValue);
        }
      };

      const attributes = {
        type: 'text',
        minimum: typeof schema.minimum === 'number' ? schema.minimum : null,
        maximum: typeof schema.maximum === 'number' ? schema.maximum : null,
        required: required === true || null,
        pattern: pattern || null,
        value: value || null,
        onChange: handleChange
      };

      console.log('made attributes', attributes);

      return h('input', attributes);
    }
  };
};
export default createComponents;
