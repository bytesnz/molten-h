import test from '../../test/ava';
const React = require('react');
const { render } = require('@testing-library/react');
import createComponents from './string';

const baseSchema = {
  type: 'string',
  title: 'Test'
};

test.serial('input() can take a null value', (t) => {
  const { input: Input } = createComponents(React.createElement);
  const { getByRole } = render(
    <Input schema={baseSchema} value={null} onChange={() => {}} />
  );
  const input = getByRole('textbox');

  t.is(input.value, '');
  t.is(input.type, 'text');
  t.false(input.hasAttribute('minLength'));
  t.false(input.hasAttribute('maxLength'));
  t.false(input.hasAttribute('pattern'));
  t.is(input.required, false);
});

test.serial('input() takes the current value and basic schema', (t) => {
  const { input: Input } = createComponents(React.createElement);
  const value = 'This is a test';
  const { getByRole } = render(
    <Input schema={{
      ...baseSchema,
      minLength: 2,
      maxLength: 10,
      pattern: '\\d+'
    }} value={value} onChange={() => {}} required={true} />
  );
  const input = getByRole('textbox');

  t.is(input.value, value);
  t.is(input.minLength, 2);
  t.is(input.maxLength, 10);
  t.is(input.pattern, '\\d+');
  t.is(input.required, true);
});

test.serial('input() should use a `type="email"` for email format', (t) => {
  const { input: Input } = createComponents(React.createElement);
  const value = 'bob@example.com';

  // email
  let { getByRole } = render(
    <Input schema={{
      ...baseSchema,
      format: 'email'
    }} value={value} onChange={() => {}} />
  );
  let input = getByRole('textbox');

  t.is(input.value, value);
  t.is(input.type, 'email');

  document.body.innerHTML = '';

  // idn-email
  ({ getByRole } = render(
    <Input schema={{
      ...baseSchema,
      format: 'idn-email'
    }} value={value} onChange={() => {}} />
  ));
  input = getByRole('textbox');

  t.is(input.value, value);
  t.is(input.type, 'email');
});
