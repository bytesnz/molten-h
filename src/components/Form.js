import React, { useState, createElement } from 'react';

import { getTypes, generateFormElements } from '../lib/generate';

const types = getTypes(createElement);

export const Form = ({value: initialValue, schema, Component}) => {
  const [value, setValue] = useState(initialValue);

  const onChange = (path, newValue) => {
    console.log('got new value', path, newValue);
    //setValue(newValue);
  };

  const elements = generateFormElements(schema, value);

  return (
    <form>
      { elementRender(elements, onChange) }
    </form>
  );
};

const elementRender = (elements, onChange) => {
  const children = [];

  for (let i = 0; i < elements.length; i++) {
    const element = elements[i];

    if (element.elements) {
    } else {
      children.push((
        <label>
          <div class="label">{element.label}</div>
          { createElement(types[element.type].input, {
            onChange: (newValue) => {
              console.log('new value', newValue);
              if (onChange) {
                onChange(element.path, newValue)
              }
            },
            required: element.required || false,
            schema: element.schema,
            value: element.value
          }) }
          <div class="description">{element.description}</div>
          { element.errors ? element.errors.map((error) => (
            <div class="errors">{error.message}</div>
          )) : null }
        </label>
      ));
    }
  }

  return children;
};
