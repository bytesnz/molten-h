import test from '../../test/ava';
const React = require('react');
const { render, wait, fireEvent, createEvent } = require('@testing-library/react');
import createComponents from './number';

const baseSchema = {
  type: 'number',
  title: 'Test'
};

test.serial('input() can take a null value', (t) => {
  const { input: Input } = createComponents(React.createElement);
  const { getByRole } = render(<Input schema={baseSchema} value={null} />);
  const input = getByRole('textbox');

  t.is(input.value, '');
  t.is(input.pattern, '\\d+(\\.\\d+)?');
});

test.serial('input() takes the current value', (t) => {
  const { input: Input } = createComponents(React.createElement);
  const value = 42;
  const { getByRole } = render(<Input schema={baseSchema} value={value} />);
  const input = getByRole('textbox');

  t.is(input.value, value.toString());
});

test.serial('input() handles minimum/maximum correctly', async (t) => {
  const { input: Input } = createComponents(React.createElement);
  const value = 42;

  // temp
  console.log('start');
  const onChange = () => {
    console.log('temp change event');
  };
  const { getByRole: tempGetByRole } = render(<input onChange={onChange} value={value} />);
  const tempInput = tempGetByRole('textbox');
  tempInput.addEventListener('change', (event) => {
    console.log('temp custom change event');
  });
  fireEvent.change(tempInput, { target: { value: '10' } });
  console.log('done');
  document.body.innerHTML = '';
  // temp

  const { getByRole } = render(<Input schema={{
    ...baseSchema,
    minimum: 10,
    maximum: 50
  }} value={value} />);
  const input = getByRole('textbox');
  console.log(input.validity);

  t.is(input.value, value.toString());

  // Minimum
  fireEvent.change(input, { target: { value: '10' } });

  await wait();
  t.is(input.value, '10');
  t.true(input.validity.valid, 'minimum value should be valid');
  input.addEventListener('change', (event) => {
    console.log('custom change event');
  });

  // Below minimum
  fireEvent.change(input, { target: { value: '9' } });

  input.dispatchEvent(new Event('change'));

  await wait();
  console.log(input.validity.customError);
  t.is(input.value, '9');
  t.false(input.validity.valid, 'validity should be invalid after setting to below minimum');

  // Maximum
  fireEvent.change(input, { target: { value: '10' } });

  t.is(input.value, '50');
  t.true(input.validity.valid, 'maximum value should be valid');

  // Above maximum
  fireEvent.change(input, { target: { value: '51' } });

  t.is(input.value, '51');
  t.false(input.validity.valid, 'validity should be invalid after setting to above maximum');
});
