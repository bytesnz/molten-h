export const createComponents = (h) => {
  return {
    input: ({ schema, value, required, onChange }) => {
      const handleChange = (event) => {
        if (onChange) {
          const newValue = event.target.checked;
          onChange(newValue);
        }
      };

      const attributes = {
        type: 'checkbox',
        required: required === true || null,
        checked: value ? true : null,
        onChange: handleChange
      };

      return h('input', attributes);
    }
  };
};
export default createComponents;

