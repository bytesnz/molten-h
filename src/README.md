molten-h <!=package version>
============================
JSON-schema-based forms

[![pipeline status](https://gitlab.com/bytesnz/first-draft/badges/master/pipeline.svg)](https://gitlab.com/bytesnz/first-draft/commits/master)
[![first-draft on NPM](https://bytes.nz/b/first-draft/npm)](https://npmjs.com/package/first-draft)
[![developtment time](https://bytes.nz/b/first-draft/custom?color=yellow&name=development+time&value=~130+hours)](https://gitlab.com/MeldCE/first-draft/blob/master/.tickings)
[![support development](https://bytes.nz/b/first-draft/custom?color=brightgreen&name=support+development&value=$$)](https://patreon.com/first_draft)
[![contributor covenant](https://bytes.nz/b/first-draft/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/MeldCE/first-draft/blob/master/code-of-conduct.md)

# INTIAL DEVELOPMENT - NOT STABLE!

**This package is under heavy initial development. Once it is ready to be
used in production, it will be published as a non-zero major version.**

## Aim

I believe that the best ideas are created through collaboration. If you work
in a visual sector, creating blueprints for buildings or layouts for web sites,
having somewhere to share your ideas, to annotate changes and highlight key
features allows fast feedback and increased user input. Everyone should have
somewhere to share their first draft.

What better place to share it than the Internet.

To make sharing and getting feedback easier, faster and more customisable,
I am creating first-draft, an open-source web-based live collaboration drawing
and annotation tool.

## Features

Some of the current and upcoming features include (_future features are in
italics_):

- synchronised drawing between clients connected to the same draft
- pen tool
- an infinite and zoomable canvas
- text annotation
- storage as JSON and images
- select tool and delete (_currently only by [ctrl +] clicking_)
- images (_currently by drag and drop or pasting and image_)
- touch screen support (_currently limited - pen tool and two-finger panning works_)
- [_user restricted editing_](https://gitlab.com/MeldCE/first-draft/issues/4)
- _configurable tools_
- [_customisable draft defaults, including available tools, permissions and tool/key mappings_](https://gitlab.com/MeldCE/first-draft/issues/27)
- [_undo/redo_](https://gitlab.com/MeldCE/first-draft/issues/8)
- [_draft versioning_](https://gitlab.com/MeldCE/first-draft/issues/10)
- [_draft duplication_](https://gitlab.com/MeldCE/first-draft/issues/9)
- [_offline drawing_](https://gitlab.com/MeldCE/first-draft/issues/23)
- [_MongoDB storage_](https://gitlab.com/MeldCE/first-draft/issues/47)
- [_Sharing your view of the draft_](https://gitlab.com/MeldCE/first-draft/issues/43)
- _Hosted Drafts_

## Demo

A demo is currently available at https://demo.first-draft.xyz

## Using

To use the package can either be installed per project, or globally

```
yarn add first-draft

# Create a folder for the drafts to be stored in
mkdir drafts

# Create a config file if you want
vim config.js

npx first-draft
```

or globally

```
yarn global add first-draft

# Create a folder for the drafts to be stored in
mkdir drafts

# Create a config file if you want
vim config.js

first-draft
```

By default, the server listens on 127.0.0.1:7067. This can be changed by
setting the `HOST` and `PORT` environment variables.

### Docker

first-draft can also be run in container on any operating system using
[Docker](https://docker.com) or similar, and the docker images available from
[first-draft's container registry](https://gitlab.com/MeldCE/first-draft/container_registry).

The `first-draft` image can be used to run the latest version of first-draft.
first-draft will start on port 80 within the container and it will by
default use file storage look for drafts in the `/app/drafts` folder. It will
also look for a configuration file in `/app/config.js`.

You can run this image using docker run:

```
docker run -i -p 8080:80 -v $(pwd)/config.js:/app/config.js -v $(pwd)/drafts:/app/drafts registry.gitlab.com/meldce/first-draft
```

The `first-draft-dev` image can be used to develop first-draft.
Again, first-draft will start on port 80, but it will run development with
both server and client hot-reloading. It will look for drafts and a `config.js`
file in the same location, but also requires the `src` folder to be mapped
onto the container at `/app/src`:

```
docker run -i -p 8080:80 -v $(pwd)/config.js:/app/config.js -v $(pwd)/drafts:/app/drafts $(pwd)/src:/app/src registry.gitlab.com/meldce/first-draft-dev
```

### Configuration

A configuration file, `config.js` can be configure some basic settings in
first-draft. The config file should be located where you start the first-draft
server from. Below is an example config file with a description of the
currently available parameters. More will come

```javascript
<!=include config.example.js>
```

### Current Controls

These are the default controls for interacting with the canvas

#### Any Tool

- Middle-button down, drag and release will pan the canvas
- Left-button down, drag and release with the shift-key will pan the canvas
- Two-finger drag will pan the canvas
- Scroll-wheel will zoom in/out the canvas
- Delete-key will delete the currently selected items

#### Pen Tool

- Left-button down, drag and release will draw a freehand line
- One-finger drag will draw a freehand line
- Left-click will start drawing lines between clicks. Right-click will finish
  the line. You can also use left-button down, drag and release to draw a
  freehand section.
- Escape during the drawing of a line will cancel the drawing of the line

#### Pan Tool

- Left-button down, drag and release will pan the canvas

#### Select Tool

- Left-click on an item will select the item
- Left-click on an item with the ctrl-key held down will add the item to
  the selection
- Left-button down, drag and release on a selected item will move the items

### Nginx Reverse Proxy

If putting first-draft behind an
[Nginx Reverse Proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/),
to be able to use Web Sockets you will need to ensure the `Upgrade` and
`Connection` headers are passed through to first-draft. If wanting to upload
images larger than 2MB, you will also need to increase the
[`client_max_body_size`](https://nginx.org/en/docs/http/ngx_http_core_module.html#client_max_body_size).
An example server configuration, with the maximum size set to 10MB is below.

```
server {
  listen 80;
  listen 443 ssl;
  server_name master.first-draft.xyz;

  client_max_body_size 10M;

  location / {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_pass http://localhost:7067;
    proxy_http_version 1.1;
    proxy_redirect off;
  }

  root /var/www/first-draft.xyz;
}
```

## Love Opensource

first-draft is built on top of some great packages, including:

- [paper.js](http://paperjs.org)
- [fontastic.me](http://fontastic.me)
- [parcel.js](https://parceljs.org)
- [prettier](https://prettier.io/)

## Contributing

If you would like to contribute, either through raising or commenting on
issues or by submitting merge requests, please read our
[Code of Conduct / Contributor Covenant](https://gitlab.com/MeldCE/first-draft/blob/master/code-of-conduct.md),
adopted from https://www.contributor-covenant.org/. Breaches of this code
will not be tolerated and will acted upon.

We greatly appreciate contributions you would like to make. Feel free to
[create a new issue](https://gitlab.com/MeldCE/first-draft/issues/new)
to suggest an improvement, request a new feature or report an
issue.

We have also set up a [Patreon page](https://patreon.com/first_draft) should
you wish to support the continued development and support of first-draft.

### Development Process

Gitlab is being used for managing the development of this projact.

- The [Feature Board](https://gitlab.com/MeldCE/first-draft/boards/982414) shows the list of features being developed
- The [Version Board](https://gitlab.com/MeldCE/first-draft/boards/982432) shows the user stories planned for each [release](https://gitlab.com/MeldCE/first-draft/milestones)
- The [Development Board](https://gitlab.com/MeldCE/first-draft/boards/971250) shows the process of user stories
- The [Bug List](https://gitlab.com/MeldCE/first-draft/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=type%3A%20bug) shows any bugs that are currently outstanding

The process that will be followed for fixing a bug/creating a new feature will
be:

1. A [Gitlab Issue](https://gitlab.com/MeldCE/first-draft/issues)
   is created for the bug/work package, detailing the requirements of what
   needs to be done.
1. The issue is assigned a priority and discussed until is is fully specified
   and ready to be developed.
1. A [Gitlab Merge Request](https://gitlab.com/MeldCE/first-draft/merge_requests)
   is with the branch named after the issue number and two or three word
   description, and marked with "WIP:" in the merge request title to symbolise
   it is not yet ready to be merged
1. Work is carried out on the branch, commiting and pushing as required
1. Once work is complete, the "WIP:" is removed from the merge request title
   and the branch will be reviewed.
1. Once reviewed for both functionality and code quality, the merge request
   will be approved
1. The merge request will then be merged into dev, ready for the next
   version release.

### Development version

To create and run a development instace of first-draft:

1. Clone the repository -
   `git clone https://gitlab.com/MeldCE/first-draft.git`
1. Install the dependencies - `npm install` or `yarn`
1. Start the development server - `npm run dev` or `yarn dev`

<!=include CHANGELOG.md>
