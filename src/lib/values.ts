
export const setValue = (item, path, value) => {
  if (!path || !path.length) {
    return value;
  }

  let part;
  let newItem = null;
  while (path.length > 1) {
    part = path.shift();
    if (Array.isArray(item)) {
      part = Number(part);

      if (isNaN(part)) {
        throw new Error('Trying to use a non-numeric index on an array');
      }

      if (!newItem) {
        newItem = item.concat();
        item = newItem;
      }
      item = item[part];
    } else if (typeof item === 'object') {
      if (!newItem) {
        newItem = Object.assign({}, item);
        item = newItem;
      }

      item = item[part];
    } else {
    }
  }

  item[path.shift()] = value;

  return newItem;
}
