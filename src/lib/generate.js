import Ajv from 'ajv';

const ajv = new Ajv();

import createStringType from '../components/string';
import createNumberType from '../components/number';
import createBooleanType from '../components/boolean';


export const getTypes = (h) => {
  return {
    string: createStringType(h),
    number: createNumberType(h),
    boolean: createBooleanType(h)
  };
}

const generateFormElementsIteration = (schema, value, errors, path) => {
  if (!path) {
    path = [];
  }

  switch (schema.type) {
    case 'string':
      return {
        label: schema.title,
        type: 'string',
        path,
        schema,
        value,
        description: schema.description || null,
        errors: (errors && errors['.' + path.join('.')]) || null
      };
    case 'number':
    case 'integer':
      return {
        label: schema.title,
        type: 'number',
        path,
        schema,
        value,
        description: schema.description || null,
        errors: (errors && errors['.' + path.join('.')]) || null
      };
    case 'boolean':
      return {
        label: schema.title,
        type: 'boolean',
        path,
        schema,
        value,
        description: schema.description || null,
        errors: (errors && errors['.' + path.join('.')]) || null
      };
    case 'object':
      let elements = [];
      if (schema.properties) {
        const properties = Object.entries(schema.properties);

        for (let i = 0; i < properties.length; i++) {
          const [ propertyId, propertySchema ] = properties[i];
          const propertyPath = [ ...path, propertyId ]
          const element = generateFormElementsIteration(
            propertySchema,
            (value && value[propertyId]) || null,
            errors,
            propertyPath,
          );

          if (Array.isArray(element)) {
            elements = elements.concat(element);
          } else {
            elements.push(element);
          }
        }
      }

      if (path.length) {
        return {
          title: schema.title,
          description: schema.description,
          elements
        };
      } else {
        return elements;
      }
    default:
  }
};

export const generateFormElements = (schema, value) => {
  const valid = ajv.validate(schema, value);
  let errors = null;
  if (!valid) {
    errors = {};
    for (let i = 0; i < ajv.errors.length; i++) {
      const key = ajv.errors[i].dataPath || '.';
      if (!errors[key]) {
        errors[key] = [ ajv.errors[i] ];
      } else {
        errors[key].push(ajv.errors[i]);
      }
    }
  }

  console.log('errors', ajv, typeof errors, errors);

  const elements = generateFormElementsIteration(schema, value, errors);

  if (!elements) {
    return null;
  }

  if (!Array.isArray(elements)) {
    return [ elements ];
  }

  return elements;
};
