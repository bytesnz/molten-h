import ava, { TestInterface } from 'ava';
const test = ava as TestInterface<{cleanup: () => void}>;
//import jsdom from 'jsdom-global';
const jsdomGlobal = require('jsdom-global');

test.beforeEach((t) => {
  t.context = {
    cleanup: jsdomGlobal()
  };
});

test.afterEach((t) => {
  t.context.cleanup();
});

export default test;
